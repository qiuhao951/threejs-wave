import Wave from './wave';

const $app = document.querySelector('#app');

const wave = new Wave({
    $container: $app
});
wave.run();