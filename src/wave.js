import * as THREE from 'three.js';

class Wave {
    constructor({ $container }) {
        this.$container = $container;
        this.positions = {
            x: 0,
            y: 0,
            z: 0,
            px: 1,
            py: 1,
            pz: 1,
            sp: .07
        },
        this.cameraConfig = {
            cameraX: 0,
            cameraY: 0,
            cameraZ: 0,
        }
        // bind this th method
        this.run = this.run.bind(this);
        // initialize
        this.init();
    }

    init () {
        const { $container } = this;
        this.WIDTH = $container.clientWidth;
        this.HEIGHT = $container.clientHeight;

        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(45, this.WIDTH / this.HEIGHT, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer();

        this.renderer.setClearColor(0xeeeeee);
        this.renderer.setSize(this.WIDTH, this.HEIGHT);

        // const axes = new THREE.AxisHelper(20);
        // this.scene.add(axes);


        this.camera.position.x = 0;
        this.camera.position.y = 0;
        this.camera.position.z = 100;
        this.camera.lookAt(this.scene.position);

        this.$container.appendChild(
            this.renderer.domElement
        );
        this.lines = new Lines({
            scene: this.scene,
            renderer: this.renderer,
            max: 25
        });
    }

    run () {
        this.render();
        window.requestAnimationFrame(this.run);
    }

    render () {
        // this.positions.x += (this.cameraConfig.cameraX * this.positions.px - this.positions.x) * this.positions.sp,
        // this.positions.y += (this.cameraConfig.cameraY * this.positions.py - this.positions.y) * this.positions.sp,
        // this.positions.z += (this.cameraConfig.cameraZ * this.positions.pz - this.positions.z) * this.positions.sp,
        // this.camera.position.x = this.positions.x,
        // this.camera.position.y = this.positions.y,
        // this.camera.position.z = this.positions.z,
        this.lines.update();
        this.renderer.render(this.scene, this.camera);
    }
}

class Lines {
    constructor({ scene, renderer, max }) {
        this.scene = scene;
        this.renderer= renderer;
        this.max = max;
        this.linesArr = [];
        this.rotationZ = 0;
        this.init();
    }
    init () {
        this.lines = new THREE.Group();
        for (let i = 0; i < lineConfig.num; i++) {
            const line = new Line(i);
            this.linesArr.push(line);
            this.lines.add(line.body);
        }
        this.scene.add(this.lines);
    }
    update() {
        this.linesArr.forEach(item => {
            item.update();
        });
        // this.rotationZ += 0.8 * 0.1;
        // this.lines.rotation.z = this.rotationZ;
    }
}

const lineConfig = {
    num: 25,
    segment: 800,
    resolution: 3,
    delay: .1,
    color: 61696
};


const waveConfig = {
    height: 15,
    gutter: 5,
    strength: .08,
    rotation: 10 * Math.PI / 180,
    speed: .02,
    randomDelay: !0
}

const circleConfig = {
    gutter: 0,
    strength: .2,
    speed: .05
}

let m = 10;
let g = 0;

class Line {
    constructor(index) {
        this.index = index,
        this.body = new THREE.Line(new THREE.Geometry(),new THREE.LineBasicMaterial({
            color: 16777215,
            opacity: 1,
            linewidth: 1,
            vertexColors: THREE.VertexColors
        })),
        this.generate(),
        this.setup(),
        this.crDefX = 4 * Math.random() - 2,
        this.crDefY = 4 * Math.random() - 2,
        this.crDefR = 6 * Math.random();
        var e = Math.floor(2 * Math.random()) ? 1 : -1
            , i = Math.floor(2 * Math.random()) ? 1 : -1;
        this.crDeltaDesX = 4 * Math.random() + 4,
        this.crDeltaDesY = 4 * Math.random() + 4,
        this.crDeltaX = 0,
        this.crDeltaY = 0,
        this.crSpeedX = .15 * e,
        this.crSpeedY = .15 * i,
        this.myWaveH = 0
        this.flag = false;
        setInterval(() => this.flag = !this.flag, 10 * 1000);
    }

    generate () {
          var t = [] , e = [];
        this.body.geometry.vertices.length = 0;
        for (var i = 0, n = lineConfig.segment; i < n; i++)
            t.push(new THREE.Vector3),
            e[i] = new THREE.Color(16777215),
            i < n / 2 ? e[i].setHSL(.32 + (.33 - .32) * i / (.5 * n), 1, .36 + (.47 - .36) * i / (.5 * n)) : e[i].setHSL(.32 + (.33 - .32) * (n - i) / (.5 * n), 1, .36 + (.47 - .36) * (n - i) / (.5 * n));
        this.body.geometry.dispose(),
        this.body.geometry = new THREE.Geometry(),
        this.body.geometry.vertices = t,
        this.body.geometry.colors = e
    }

    setup () {
        this.randomWave = Math.random() * Math.PI,
        this.randomWave = Math.floor(100 * this.randomWave) / 100,
        this.delayWave = this.index * lineConfig.delay,
        this.randomWave2 = Math.random() * Math.PI
    }

    update () {
        this.flag ? this.wave() : this.circle();
        this.body.geometry.verticesNeedUpdate = true;
    }

    wave () {
        this.randomWave += waveConfig.speed;
        this.desWaveH = m;
        this.myWaveH += .05 * (this.desWaveH - this.myWaveH);
        const t = this.index * waveConfig.gutter - (lineConfig.num - 1) * waveConfig.gutter * .5;
        const e = -lineConfig.resolution * lineConfig.segment * .5;
        const n = this.body.geometry.vertices.length;
        for (let i = 0; i < n; i++) {
            let r = Math.sin(this.randomWave + i * waveConfig.strength) * Math.cos(this.randomWave + this.randomWave2);
            let o = this.body.geometry.vertices[i];
            let s = (this.index - .5 * lineConfig.num) * g;
            let l = t;
            let h = r * this.myWaveH - s;
            let p = i * lineConfig.resolution + e;
            let f = l - o.z;
            let d = h - o.y;
            let v = p - o.x;
            o.z += .08 * f;
            o.y += .08 * d;
            o.x += .08 * v;
        }
        this.body.rotation.z = waveConfig.rotation,
        this.body.rotation.x = 0
    }

    circle () {
        this.randomWave += circleConfig.speed;
        var t = this.index * circleConfig.gutter - (lineConfig.num - 1) * circleConfig.gutter * .5;
        (this.crDeltaX < -this.crDeltaDesX || this.crDeltaX > this.crDeltaDesX) && (this.crSpeedX *= -1),
        (this.crDeltaY < -this.crDeltaDesY || this.crDeltaY > this.crDeltaDesY) && (this.crSpeedY *= -1),
        this.crDeltaX = this.crDeltaX + this.crSpeedX,
        this.crDeltaY = this.crDeltaY + this.crSpeedY;
        for (var e = 0, i = this.body.geometry.vertices.length; e < i; e++) {
            var n = (Math.sin(this.randomWave + e * circleConfig.strength),
                Math.cos(this.randomWave + e * circleConfig.strength),
                this.body.geometry.vertices[e])
            const r = Math.PI / 180;
            const s = 360 / (this.body.geometry.vertices.length - 1) * e * r + this.crDefR;
            const l = t;
            const h = 40 * Math.sin(s) + this.crDefX + this.crDeltaY;
            const u = 40 * Math.cos(s) + this.crDefY + this.crDeltaX;
            const p = l - n.z;
            const f = h - n.y;
            const m = u - n.x;
            n.z += .08 * p;
            n.y += .08 * f;
            n.x += .08 * m;
        }
        this.body.rotation.z = 0,
        this.body.rotation.x = 0
    }
}

export default Wave;